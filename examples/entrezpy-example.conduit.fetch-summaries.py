#!/usr/bin/env python3
"""
.. module:: entrezpy-example.conduit.fetch-summaries
  :synopsis:
    Example of using entrezpy's Conduit to run a summary pipeline

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>

..
  Copyright 2018 The University of Sydney

  Demonstrate Entrezpy's Conduit class and setup. Conduit facilitates the
  creation of E-Utility query pipelines [0].

  The examples are stored as parameters in the list `examples` (taken from [0]).
  Outline
  -------
  0. Import entrezpy
  1. Create a Conduit instance  with the required parameters:
      - instance name
      - user email.
      These are required by NCBI [1]. The instance name corresponds to the
      Eutils `tool` parameter [1].
  2. Get a new Conduit pipeline
  3. Add queries to the pipline
  4. Run the pipeline
  3. Print specific attributes form the summaries to STDOUT

  Setup
  -----
  Set the proper import path to the required classes relative to this file by
  updating ``sys.payth``. The example assumes you cloned the git repository from
  https://gitlab.com/ncbipy/entrezpy.git.

::
  $reporoot
  |-- examples
  |   `-- entrezpy-examples.conduit.fetch-summaries.py  <-You are here
  `-- src
      `-- entrezpy
          `-- efetch
              |-- efetch_analyzer.py
              `-- efetch.py

  N.B.
    NCBI api key[1]: If an apikey is passed to Efetcher(), it will be used to
                    allow more requests [1]. Without apikey, Entrezpy checks if
                    the environmental variable $NCBI_API_KEY is set. If not,
                    less queries per second are performed.

    Efetch analyzer:  Efecth is unique in respect to all other functions as its
                      function `inquire` requires an analyzer as parameter. This
                      example uses the default analyzer which prints either the
                      errors or stores the result as string to
                      `analyzer.result`. It can be easly adapted for more
                      complex tasks (see documentation).
  References:
    [0]: https://www.ncbi.nlm.nih.gov/books/NBK25499/#chapter4.EFetch
    [1]: https://www.ncbi.nlm.nih.gov/books/NBK25497/#chapter2.Usage_Guidelines_and_Requirement
    [2]: https://docs.python.org/3/library/argparse.html#module-argparse
"""

import os
import sys
import json
import time
import argparse

sys.path.insert(1, os.path.join(sys.path[0], '../src'))
# Import conduit module
import entrezpy.conduit

entrezpy.log.logger.set_level('INFO')

def main():
  ap = argparse.ArgumentParser(description='Simple ncbipy-esearch-example')
  ap.add_argument('--email',
                  type=str,
                  required=True,
                  help='email required by NCBI'),
  ap.add_argument('--apikey',
                  type=str,
                  default=None,
                  help='NCBI apikey (optional)')
  ap.add_argument('--apikey_envar',
                  type=str,
                  default=None,
                  help='Environment varriable storing NCBI apikey (optional)')
  ap.add_argument('--threads',
                  type=int,
                  default=0,
                  help='number of threads to use (default=no threads)')
  ap.add_argument('--use_history',
                  default=False,
                  action='store_true',
                  help='Run example using NCBI history server for linking')

  args = ap.parse_args()

  start = time.time()
  # Init a Conduit instance
  w = entrezpy.conduit.Conduit(args.email, args.apikey, args.apikey_envar, threads=args.threads)
  # Create new Conduit pipeline summary_pipeline
  summary_pipeline = w.new_pipeline()
  # Add search query
  pid = summary_pipeline.add_search({'db' : 'gene',
                       'term' : 'tp53[preferred symbol] AND human[organism]',
                       'rettype' : 'count'})
  # Use history server if requested
  if args.use_history:
    pid = summary_pipeline.add_link({'db' : 'protein', 'cmd':'neighbor_history'}, dependency=pid)
    pid = summary_pipeline.add_search(dependency=pid)
  else:
    # Get UIDs from link
    pid = summary_pipeline.add_link({'db' : 'protein'}, dependency=pid)
  # Add summary query to pipeline
  pid = summary_pipeline.add_summary(dependency=pid)
  # Run pipeline and get default analyzer
  analyzer = w.run(summary_pipeline)
  # Show summaries. Summaries are stored as dictionary {UID:summary} to be
  # accessed quickly. To loop over them, use the following approach
  for uid, summary in analyzer.get_result().summaries.items():
    print("{}\t{}".format(uid, summary.get('caption'),summary.get('sourcedb')))
  print("Threads: {}\tSummaries: {}\nDurations: {} [s]".format(w.threads,
                                                len(analyzer.result.summaries),
                                                 time.time()-start))
  return 0

if __name__ == '__main__':
  main()
