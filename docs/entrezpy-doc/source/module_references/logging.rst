.. _logging:

Logging module
==============

Functions
---------

.. automodule:: entrezpy.log.logger
  :members:

Configuration
-------------

.. automodule:: entrezpy.log.conf
  :members:
  :undoc-members:

